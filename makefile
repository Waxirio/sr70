all: main clean

main: main.o main.o alloc.o cpu.o
	gcc -o main main.o alloc.o cpu.o -pthread

main.o: main.c 
	gcc -o main.o -c main.c -pthread

alloc.o: alloc.c 
	gcc -o alloc.o -c alloc.c -pthread

cpu.o: cpu.c 
	gcc -o cpu.o -c cpu.c -pthread

clean:
	rm -rf *.o