#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <semaphore.h>
#include <sys/stat.h>

#include "cpu.h"

long current_prio = 1;

//external variable
extern int new_proc;
extern int exec_proc;
extern int nb_processus;
extern sem_t mutexA;
extern sem_t mutexB;
extern int current_time;
extern long quantum;
extern int att_time;
extern int chance_proc;
extern int procenable;

void attente(int n) {
   sleep(n);
}

// Crée une nouvelle tache avec les parametres necessaire
Task createTask(int id, long priorite, int tempsExec){
    //nouvelle tache
    Task res;
    //attribution des params aux bonnnes proprietes
    res._id = id;
    res._priorite = priorite;
    res._tempsExec = tempsExec;
    //retourne la tache
    return res;
}

// Crée une nouvelle tache chargement avec les parametres necessaire
LoadedTask createLoadedTask(Task t, long dateDebut){
    //nouvelle tache
    LoadedTask res;
    res._task = t;
    res._dateDebut = dateDebut;
    //retourne la tache
    return res;
}

/* Execute une tache */
int exec_task(Task t, int quantum_remaining){
    if(t._tempsExec > quantum_remaining){
        //On met a jour le temps d'exec
        t._tempsExec -= quantum_remaining;
        //On met a jour le quantum restant
        quantum_remaining = 0;

        //On augmente la priorité
        t._priorite ++;
        //Si celle ci est suppérieur à 10, on la reset à 1
        if(t._priorite > 10){
            t._priorite = 1;
        }
        printf("cpu: execution de %d, reste %d\n", t._id, t._tempsExec);
        //Si il est pas terminé on le remet dans la file
        if(msgsnd(exec_proc, &t, sizeof(Task), IPC_NOWAIT) == -1){
            printf("Bug lors de l'ajout de la tache après execution\n");
        }
        else{
            printf("cpu: retour de la tache %d\n", t._id);
        }
    }
    else{
        //Sinon
        quantum_remaining -= t._tempsExec;
        //Le proc a fini d'etre execute
        t._tempsExec = 0;
        printf("cpu: execution de %d, reste %d\n", t._id, t._tempsExec);
        //Fin du processus
        printf("cpu: fin de %d\n", t._id);
    }
    //On retourne le quantum
    return quantum_remaining;
}

/* Ajoute un nouveau processus */
void add_new_task(){
    int alea = (rand() % (100 - 1)) + 1;
    if(alea <= chance_proc){
        long prio = (rand() % (10 - 1)) + 1;
        int tps_exec = (rand() % (20 - 1)) + 1;
        long date_deb = (rand() % (10 - 1)) + current_time;
        nb_processus ++;
        LoadedTask t = createLoadedTask(createTask(nb_processus, prio, tps_exec), date_deb);
        if(msgsnd(new_proc, &t, sizeof(LoadedTask), IPC_NOWAIT) == -1){
            struct msqid_ds buf;
            msgctl(new_proc, IPC_STAT, &buf);
            printf("NEW messages : %u   ||   EXEC Nombre max: %u\n", (uint)buf.msg_cbytes, (uint)buf.msg_qbytes);
            msgctl(exec_proc, IPC_STAT, &buf);
            printf("EXEC messages : %u   ||   EXEC Nombre max: %u\n", (uint)buf.msg_cbytes, (uint)buf.msg_qbytes);
            printf("echec de l'ajout de la nouvelle tache\n");
        }
        else{
            printf("Ajout d'une nouvelle tache %d, date de debut: %ld\n", nb_processus, t._dateDebut);
        }
    }
}

/*************************/
/* Fonction pour le CPU. */
/*************************/
void* Cpu (void * args)
{
    //Variables de cpu
    Task t;
    int quantum_remaining;
    long last_prio = 1;
    int scan;
    //boucle du cpu
    while(1){
        //generation d'un nouveau processus
        attente(att_time);
        printf("\n ***** Prio actuelle: %ld - date actuelle: %d *****\n", current_prio, current_time);
        sem_post(&mutexA);
        if(procenable){
            add_new_task();
        }
        sem_wait(&mutexB);
        //On initialise le quantum actuelle à la valeur donné
        quantum_remaining = quantum;
        scan = 1;
        //Tant qu'on a pas fini de pouvoir executer un processus
        while(quantum_remaining > 0 && scan != 10){
            //On recupere les processus de prio 'current_prio'
            if(msgrcv(exec_proc, &t, sizeof(Task), current_prio, IPC_NOWAIT) != -1){
                //affichage de l'exec
                printf("cpu: election de %d\n", t._id);
                //Si on trouve un proc de la bonne prio on l'execute
                quantum_remaining = exec_task(t, quantum_remaining);
            }
            else{
                //On passe a la priorité suivante
                current_prio ++;
                //Si on arrive au max on passe à 1
                if(current_prio > 10){
                    current_prio = 1;
                }
                scan ++;
            }
        }
        current_time ++;  
        last_prio ++;
        if(last_prio > 10){
            last_prio = 1;
        }
        current_prio = last_prio;      
    }

    return NULL;
}