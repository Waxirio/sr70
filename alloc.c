#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <semaphore.h>
#include <sys/stat.h>

#include "alloc.h"

//external variable
extern int new_proc;
extern int exec_proc;
extern sem_t mutexA;
extern sem_t mutexB;
extern int current_time;
extern int att_time;

/****************************************/
/* Fonction pour la table d'allocation. */
/****************************************/
void* Table_alloc (void * args)
{
    long deb_task;
    LoadedTask lt;
    Task t;
    while(1){
        sem_wait(&mutexA);
        while(msgrcv(new_proc, &lt, sizeof(LoadedTask), current_time, IPC_NOWAIT) != -1){
            t = lt._task;
            deb_task = lt._dateDebut;
            if(msgsnd(exec_proc, &t, sizeof(Task), IPC_NOWAIT) == -1){
                printf("alloc: echec de la transmission du message\n");
            }
            else{
                printf("alloc: ajout de %d dans la file d'execution\n\tdate debut: %ld\n\tprio: %ld\n\ttemps d'exec: %d\n", t._id, deb_task, t._priorite, t._tempsExec);
            }
        }
        sem_post(&mutexB);
    }

    return NULL;
}