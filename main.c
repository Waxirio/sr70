// include lib
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <signal.h>

// include external files
#include "main.h"

/* Semaphore */
#define NBSEM 2
#define SKEY   (key_t) IPC_PRIVATE	
#define IFLAGS (SEMPERM | IPC_CREAT)
#define SEMPERM 0600

long quantum;
int current_time = 1;

pthread_t th_cpu;
pthread_t th_alloc;

// external variable
int new_proc;
int exec_proc;
int nb_processus;
int att_time;
int chance_proc;
int procenable;
sem_t mutexA;
sem_t mutexB;

//convert char into int
int digit_to_int(char d)
{
    char str[2];
    str[0] = d;
    str[1] = '\0';
    return (int) strtol(str, NULL, 10);
}

//Charge les tâches du fichier
void loadAllTasks(){

    static const char filename[] = "data";
    FILE *file = fopen ( filename, "r" );
    LoadedTask t;

    if ( file == NULL )
    {
        printf( "Impossible de lire le fichier de taches\n" ) ;
        exit(1);
    }
    //Creation des elements 
    char line [ 16 ];
    int i = 0;
    char delim[] = ";";    
    while(fgets(line, 16, file) != NULL)
    {
        char *ptr = strtok(line, delim);
        int id = digit_to_int(*ptr);
        int prio = digit_to_int(*strtok(NULL, delim));
        int date_deb = digit_to_int(*strtok(NULL, delim));
        int tps_exec = digit_to_int(*strtok(NULL, delim));
        t = createLoadedTask(createTask(id, (long)prio, tps_exec), date_deb);
        if(msgsnd(new_proc, &t, sizeof(LoadedTask), 1) != -1){
            printf("Chargement tache %d\n", id);
            nb_processus ++;
        }
    }

    free(file);
}

void displayTask(Task t){
    printf("Tache %d :\n priorite: %ld\n temps d'execution %d\n", t._id, t._priorite, t._tempsExec);
}

void createFile(){
    int id; //valeur temporaire pour les files de message

    //Creation de la premiere file
    if((id = msgget(123, 0750 | IPC_CREAT | IPC_EXCL)) == -1) {
        perror("Erreur de creation de la file\n");
        exit(1);
    }
    //Creation de la file des nouveaux processus
    new_proc = id;
    printf("Creation file arrive %d\n", id);

    //Creation de la seconde cle
    if((id = msgget(1234, 0750 | IPC_CREAT | IPC_EXCL)) == -1) {
        perror("Erreur de creation de la file\n");
        exit(1);
    }
    //Creation de la file des processus a executer
    exec_proc = id;
    printf("Creation file exec %d\n", id);
}

void sigintHandler(int sig_num) 
{ 
    void *retour; //Valeur retour des thread

    signal(SIGINT, sigintHandler);

    //On attends la fin des threads
    pthread_cancel(th_cpu);
    pthread_join(th_cpu, retour);
    pthread_cancel(th_alloc);
    pthread_join(th_alloc, retour);

    //Suppression des files de messages
    if (msgctl(new_proc, IPC_RMID, NULL) == -1) {
        fprintf(stderr, "Erreur dans la suppression de la file des processus entrantes.\n");
        exit(EXIT_FAILURE);
    }
    if (msgctl(exec_proc, IPC_RMID, NULL) == -1) {
        fprintf(stderr, "Erreur dans la suppression de la file des processus de taches.\n");
        exit(EXIT_FAILURE);
    }

    //On detruit les semaphores
    if(sem_destroy(&mutexA)){
        fprintf(stderr, "Erreur dans la suppression du semaphore A.\n");
        exit(EXIT_FAILURE);
    }
    if(sem_destroy(&mutexB)){
        fprintf(stderr, "Erreur dans la suppression du semaphore B.\n");
        exit(EXIT_FAILURE);
    }

    free(retour);

    printf("\n\n > Fin de l'execution\n");

    exit(EXIT_SUCCESS);

    fflush(stdout); 
} 

//main function
int main(int argc, char *argv[])
{
    //Les variables main
    Task t;
    key_t key;

    //La structure de semaphore
    union semun {
		int val;
		struct semid_ds *buf;
		short * array;
	} arg;

    char p;
    int choix;

    //Recuperation du quantum
    quantum = 5;
    printf("Quel est le quantum souhaité ? \n");
    scanf("%ld", &quantum);

    printf("Temps d'attente entre chaque quantum en seconde ? \n");
    scanf("%d", &att_time);

    printf("Ajout de nouveaux processus aléaoire en cours de programme ? (y/n)\n");
    scanf("%s", &p);
    if(p == 'y'){
        procenable = 1;
        printf("Pourcentage de chance d'ajout d'un processus aléatoire [0-100]\n");
        scanf("%d", &chance_proc);
    }
    else{
        procenable = 0;
    }

    printf("1. Utiliser le jeu de test ?\n");
    printf("2. Entrer les processus ?\n");
    scanf("%d", &choix);

    //Creation des files de messages
    createFile();

    if(choix == 1){
        //Lit les taches dans le fichier
        loadAllTasks();
    }
    else if(choix == 2){
        printf("Nombre de processus ?\n");
        scanf("%d", &nb_processus);
        int id, tempsexec;
        long datedeb, priorite;
        LoadedTask t;

        for (int i=0; i<nb_processus; i++){
            id = i;
            printf("----- Nouvelle tache -----\n");
            printf("Date de debut ?");
            scanf("%ld", &datedeb);
            printf("Priorite ?");
            scanf("%ld", &priorite);
            printf("Temps d'execution ?");
            scanf("%d", &tempsexec);
            t = createLoadedTask(createTask(id, priorite, tempsexec), datedeb);
            if(msgsnd(new_proc, &t, sizeof(LoadedTask), 1) != -1){
                printf("Chargement tache %d\n", id);
            }
        }
    }

    //Pour le random
    srand(time(NULL));

    //Creation des semaphores
    if(sem_init(&mutexA, 1, 0)){
        printf("error sem A");
    }
    if(sem_init(&mutexB, 1, 0)){
        printf("error sem B");
    }

    //Affichage de départ
    printf("Valeur du quantum = %ld\n", quantum);
    printf("Nombre de processus = %d\n", nb_processus);

    //On cree les threads
    if(pthread_create(&th_alloc, NULL, Table_alloc, NULL) != 1){
        printf("depart table d'alloc\n");
    }
    if(pthread_create(&th_cpu, NULL, Cpu, NULL) != -1){
        printf("depart cpu\n");
    }

    //wait for sig
    signal(SIGINT, sigintHandler);
    while(1){

    }

    return 0;
}