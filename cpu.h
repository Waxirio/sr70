#ifndef CPU_H
#define CPU_H

/*
cpu.h
*/

#include "task.h"

void attente(int n);
LoadedTask createLoadedTask(Task t, long dateDebut);
Task createTask(int id, long priorite, int tempsExec);
int exec_task(Task t, int quantum_remaining);
void add_new_task();
void* Cpu (void * args);

#endif