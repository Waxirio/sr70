#ifndef MAIN_H
#define MAIN_H

/*
main.h
*/

#include "alloc.h"
#include "cpu.h"

void createFile();
int digit_to_int(char d);
void displayTask(Task t);
void loadAllTasks();
int main(int argc, char *argv[]);

#endif

