#ifndef TASK_H
#define TASK_H

/*
task.h
*/

typedef struct
{
    long _priorite;
    int _id;
    int _tempsExec;
} Task;

typedef struct
{
    long _dateDebut;
    Task _task;
} LoadedTask;

#endif